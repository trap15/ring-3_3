OBJECTS = pck.o main.o
OUTPUT = ring-3_3
CFLAGS = -Wall -pedantic -g -Werror
LDFLAGS = 

all: $(OUTPUT)
%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<
$(OUTPUT): $(OBJECTS)
	$(CC) $(LDFLAGS) -o $(OUTPUT) $(OBJECTS)
clean:
	$(RM) $(OUTPUT) $(OBJECTS)
