/*
Copyright (C) 2010              Alex Marshall "trap15" <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int extract_pck(FILE* fp, char *outdir);

void usage(char *app)
{
	fprintf(stderr, "Invalid arguments. Usage:\n"
			"	%s in.pck outdir\n", app);
}

int main(int argc, char *argv[])
{
	FILE* fp;
	if(argc < 3) {
		usage(argv[0]);
		return EXIT_FAILURE;
	}
	fp = fopen(argv[1], "rb");
	if(fp == NULL) {
		perror("unable to open input file");
		return EXIT_FAILURE;
	}
	if(extract_pck(fp, argv[2]) == 0)
		return EXIT_SUCCESS;
	else {
		perror("unable to extract pck");
		return EXIT_FAILURE;
	}
}


