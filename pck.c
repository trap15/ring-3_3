/*
Copyright (C) 2010              Alex Marshall "trap15" <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <sys/stat.h>
#include <arpa/inet.h>

#ifdef __WIN32__
#  define compat_mkdir(path)	mkdir(path);
#else
#  define compat_mkdir(path)	mkdir(path, S_IRWXU);
#endif

#define es16(x)	((((x) & 0x00FF) << 8) | \
		 (((x) & 0xFF00) >> 8))
#define es32(x)	((((x) & 0x000000FF) << 24) | \
		 (((x) & 0x0000FF00) <<  8) | \
		 (((x) & 0x00FF0000) >>  8) | \
		 (((x) & 0xFF000000) >> 24))

#define be32(x)	htonl(x)
#define be16(x)	htons(x)
#define le32(x)	es32(be32(x))
#define le16(x)	es16(be16(x))

int read_chunk(FILE* fp, char* outdir)
{
	int i;
	int size = 0x20;
	FILE* ofp;
	char *fbuf;
	char *outfn;
	/* Actually wchar! */
	char fn[0x20];
	char hackfn[0x20];
	uint32_t chunk, unk;
	/* Unknown */
	fread(&unk, 4, 1, fp);
	unk = le32(unk);
	/* Chunk size */
	fread(&chunk, 4, 1, fp);
	chunk = le32(chunk);
	/* Filename (wchar) */
	for(i = 0; i < size; i++) {
		fread(hackfn + i, 1, 1, fp);
		if(hackfn[i] == '.') /* Derp */
			size = i + 8;
	}
	for(i = 0; i < size; i++) {
		fn[i] = 0;
		if(!(i & 1))
			fn[i >> 1] = hackfn[i];
	}
	fflush(stdout);
	printf("Extracting %s\n", fn);
	asprintf(&outfn, "%s/%s", outdir, fn);
	ofp = fopen(outfn, "wb");
	if(ofp == NULL) {
		perror("unable to open output file");
		return 0;
	}
	fbuf = malloc(chunk);
	fread(fbuf, chunk, 1, fp);
	fwrite(fbuf, chunk, 1, ofp);
	free(fbuf);
	fclose(ofp);
	return 1;
}

int extract_pck(FILE* fp, char* outdir)
{
	uint32_t tmp32[4];
	fread(tmp32 + 0, 4, 1, fp);
	tmp32[0] = be32(tmp32[0]);
	if(tmp32[0] != 0x2E70636B) { /* .pck */
		fprintf(stderr, "Not a pck file.\n");
		return 1;
	}
	compat_mkdir(outdir);
	for(;;) {
		fgetc(fp);
		if(feof(fp))
			break;
		fseek(fp, -1, SEEK_CUR);
		if(!read_chunk(fp, outdir))
			return 1;
	}
	return 0;
}

